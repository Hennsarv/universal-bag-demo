﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace ConsoleApp21
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, object> TestData = new Dictionary<string, object>();
            dynamic TestBag = new UniversalBag(TestData);

            TestData["Nimi"] = "Henn";
            TestData["Vanus"] = 63;

            TestBag.Vanus++;
            TestBag.Vanus -= 20;
            Console.WriteLine($"Nimi: {TestBag.Nimi} vanus: {TestBag.Vanus}");

        }
    }

    class UniversalBag : DynamicObject
    {
        Dictionary<string, object> Prop;

        public UniversalBag(Dictionary<string, object> prop)
        {
            Prop = prop;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = Prop.Keys.Contains(binder.Name) ?
                    Prop[binder.Name] : null;
            return true;
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Prop[binder.Name] = value;
            return true;
        }
    }
}
